package com.apgblogs.springbootstudy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-08 12:00
 */
@Controller
public class IndexController {

    /**
     * @description 首页
     * @author xiaomianyang
     * @date 2019-07-08 12:13
     * @param []
     * @return java.lang.String
     */
    @GetMapping("index")
    public String index(){
        return "index";
    }
}
