package com.apgblogs.springbootstudy.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-06-01 12:37
 */
@RestController
public class HelloController {

    @GetMapping("/hello")
    public String index(){
        return "hello world";
    }

}
