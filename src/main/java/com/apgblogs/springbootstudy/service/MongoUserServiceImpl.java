package com.apgblogs.springbootstudy.service;

import com.apgblogs.springbootstudy.entity.MongoUser;
import com.apgblogs.springbootstudy.repository.MongoDbUserRepository;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-06-30 18:32
 */
@Service
public class MongoUserServiceImpl {

    @Autowired
    private MongoTemplate mongoTemplate=null;

    @Autowired
    private MongoDbUserRepository mongoDbUserRepository;

    /**
     * @description 通过objectId查询用户
     * @author xiaomianyang
     * @date 2019-07-01 09:57
     * @param [id]
     * @return com.apgblogs.springbootstudy.entity.MongoUser
     */
    public MongoUser getUser(String id){
        return mongoDbUserRepository.findById(id).get();
//        return mongoTemplate.findById(id,MongoUser.class);
//        Criteria criteria=Criteria.where("user_name").is(id);
//        Query query=Query.query(criteria);
//        return mongoTemplate.findOne(query,MongoUser.class);
    }

    /**
     * @description 通过筛选条件查询用户
     * @author xiaomianyang
     * @date 2019-07-01 09:57
     * @param [userName, note, skip, limit]
     * @return java.util.List<com.apgblogs.springbootstudy.entity.MongoUser>
     */
    public List<MongoUser> findUser(String userName,String note,int skip,int limit){
        Criteria criteria=Criteria.where("userName").regex(userName)
                .and("note").regex(note);
        Query query=Query.query(criteria).limit(limit).skip(skip);
        List<MongoUser> mongoUserList=mongoTemplate.find(query,MongoUser.class);
        return mongoUserList;
    }

    /**
     * @description 保存用户
     * @author xiaomianyang
     * @date 2019-07-01 09:58
     * @param [mongoUser]
     * @return void
     */
    public MongoUser saveUser(MongoUser mongoUser){
        MongoUser mongoUser1=mongoTemplate.save(mongoUser);
        return mongoUser1;
    }

    /**
     * @description 通过Id删除用户
     * @author xiaomianyang
     * @date 2019-07-01 10:24
     * @param [id]
     * @return com.mongodb.client.result.DeleteResult
     */
    public DeleteResult deleteUser(String id){
        Criteria criteria=Criteria.where("id").is(id);
        Query query=Query.query(criteria);
        DeleteResult deleteResult=mongoTemplate.remove(query,MongoUser.class);
        return deleteResult;
    }

    /**
     * @description 更新用户
     * @author xiaomianyang
     * @date 2019-07-01 10:28
     * @param [id, userName, note]
     * @return com.mongodb.client.result.UpdateResult
     */
    public UpdateResult updateUser(String id,String userName,String note){
        Criteria criteria=Criteria.where("id").is(id);
        Query query=Query.query(criteria);
        Update update=Update.update("userName",userName);
        update.set("note",note);
        UpdateResult updateResult=mongoTemplate.updateFirst(query,update,MongoUser.class);
        return updateResult;
    }
}
