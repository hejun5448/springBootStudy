package com.apgblogs.springbootstudy.service;

import com.apgblogs.springbootstudy.base.BaseService;
import com.apgblogs.springbootstudy.dao.UserDao;
import com.apgblogs.springbootstudy.entity.TUser;
import com.apgblogs.springbootstudy.entity.TUserEntity;
import com.apgblogs.springbootstudy.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-06-13 10:35
 */
@Service
public class UserServiceImpl extends BaseService<UserDao, TUser> {

    @Autowired
    private UserRepository userRepository;

    /**
     * @description 查询所有用户
     * @author xiaomianyang
     * @date 2019-06-13 10:37
     * @param []
     * @return java.util.List<TUserEntity>
     */
    public List<TUserEntity> getUserList(){
        return userRepository.findAll();
    }

    /**
     * @description 查询单个用户
     * @author xiaomianyang
     * @date 2019-06-13 10:39
     * @param [id]
     * @return TUserEntity
     */
    @Cacheable(value="redisCache",condition = "#result!='null'",key="'redis_user_'+#id")
    public TUserEntity getUser(String id){
        if(!userRepository.existsById(id)){
            return null;
        }
        return userRepository.findById(id).get();
    }

    /**
     * @description 创建用户
     * @author xiaomianyang
     * @date 2019-06-13 10:45
     * @param [tUserEntity]
     * @return TUserEntity
     */
    @CachePut(value="redisCache",key="'redis_user_'+#result.id")
    public TUserEntity insertUser(TUserEntity tUserEntity){
        tUserEntity.setCreateBy("sys");
        tUserEntity.setUpdateBy("sys");
        return userRepository.save(tUserEntity);
    }

    /**
     * @description 修改用户
     * @author xiaomianyang
     * @date 2019-06-13 10:40
     * @param [tUserEntity]
     * @return TUserEntity
     */
    @CachePut(value="redisCache",condition = "#result!='null'",key="'redis_user_'+#tUserEntity.id")
    public TUserEntity updateUser(TUserEntity tUserEntity){
        TUserEntity tUserEntity1=userRepository.findById(tUserEntity.getId()).get();
        if(tUserEntity1==null){
            return null;
        }
        tUserEntity1.setUserName(tUserEntity.getUserName());
        tUserEntity1.setUserPswd(tUserEntity.getUserPswd());
        return userRepository.save(tUserEntity1);
    }

    /**
     * @description 删除用户
     * @author xiaomianyang
     * @date 2019-06-13 10:44
     * @param [id]
     * @return void
     */
    @CacheEvict(value = "redisCache",key="'redis_user_'+#id",beforeInvocation = false)
    public boolean deleteUser(String id){
        try{
            userRepository.deleteById(id);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
