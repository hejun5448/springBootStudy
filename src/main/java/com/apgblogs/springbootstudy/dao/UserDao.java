package com.apgblogs.springbootstudy.dao;

import com.apgblogs.springbootstudy.base.BaseDao;
import com.apgblogs.springbootstudy.entity.TUser;

public interface UserDao extends BaseDao<TUser> {
}