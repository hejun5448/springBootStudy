package com.apgblogs.springbootstudy.validator;

import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-10 17:12
 */
public class UserNameValidator implements ConstraintValidator<UserNameNotExist,String> {

    @Override
    public void initialize(UserNameNotExist constraintAnnotation) {
    }

    @Override
    public boolean isValid(String username, ConstraintValidatorContext constraintValidatorContext) {
        if(StringUtils.isEmpty(username)){
            return true;
        }
        if(username.equals("Java")){
            return true;
        }
        return false;
    }
}
