package com.apgblogs.springbootstudy.repository;

import com.apgblogs.springbootstudy.entity.MongoUser;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-01 10:38
 */
public interface MongoDbUserRepository extends MongoRepository<MongoUser,String> {
}
