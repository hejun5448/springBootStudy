package com.apgblogs.firstspringboot;

import com.apgblogs.springbootstudy.SpringBootStudyApplication;
import com.apgblogs.springbootstudy.dao.UserDao;
import com.apgblogs.springbootstudy.entity.MongoRole;
import com.apgblogs.springbootstudy.entity.MongoUser;
import com.apgblogs.springbootstudy.entity.SysUserEntity;
import com.apgblogs.springbootstudy.entity.TUser;
import com.apgblogs.springbootstudy.entity.TUserEntity;
import com.apgblogs.springbootstudy.service.MongoUserServiceImpl;
import com.apgblogs.springbootstudy.service.UserServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.google.gson.Gson;
import com.mongodb.Mongo;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import javafx.application.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootStudyApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpringBootStudyApplicationTests {

    private static Logger logger= LoggerFactory.getLogger(SpringBootStudyApplicationTests.class);

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private MongoUserServiceImpl mongoUserService;

    @Test
    public void getUserList(){
        List<TUserEntity> tUserEntityList=userService.getUserList();
        logger.info("用户列表{}",new Gson().toJson(tUserEntityList));
    }

    @Test
    public void insertUser(){
        TUserEntity tUserEntity=new TUserEntity();
        tUserEntity.setUserName("赵四");
        tUserEntity.setUserPswd("123456");
        tUserEntity.setCreateBy("123");
        tUserEntity.setUpdateBy("123");
        userService.insertUser(tUserEntity);
    }

    @Test
    public void getUser(){
        TUserEntity tUserEntity=userService.getUser("4028f1816b7d66d9016b7d6be1340000");
        logger.info("用户{}",new Gson().toJson(tUserEntity));
    }

    @Test
    public void getAllUser(){
        Page page=new Page();
        //每页数量
        page.setPageSize(10);
        //页码
        page.setPageNum(0);
        //排序 排序可以放到page中，在查询语句中可以动态支持排序
        page.setOrderBy("");
        PageInfo pageInfo=userService.selectAll(null,page);
        logger.info("分页用户,第{}页，共{}条，{}",pageInfo.getPageNum(),pageInfo.getTotal(),pageInfo.getList());
    }

    @Test
    public void getMongoUser(){
        logger.info("mongoDb用户{}",new Gson().toJson(mongoUserService.getUser("1")));
    }

    @Test
    public void getMongoUserByCondition(){
        logger.info("用户{}",new Gson().toJson(mongoUserService.findUser("小绵羊","筑梦师",0,5)));
    }

    @Test
    public void saveMongoUser(){
        MongoUser mongoUser=new MongoUser();
        mongoUser.setId("1");
        mongoUser.setUserName("亚索");
        mongoUser.setNote("托儿索");
        MongoRole mongoRole=new MongoRole();
        mongoRole.setId("3");
        mongoRole.setRoleName("中单");
        mongoRole.setNote("快乐风男");
        List<MongoRole> mongoRoleList=new ArrayList<MongoRole>();
        mongoRoleList.add(mongoRole);
        mongoUser.setRoleList(mongoRoleList);
        logger.info("新增用户{}",new Gson().toJson(mongoUserService.saveUser(mongoUser)));
    }

    @Test
    public void updateMongoUser(){
        UpdateResult updateResult=mongoUserService.updateUser("1","盲僧","瞎子");
        logger.info("更新用户{}",updateResult.getModifiedCount());
    }

    @Test
    public void deleteMongoUser(){
        DeleteResult deleteResult=mongoUserService.deleteUser("1");
        logger.info("删除用户{}",deleteResult.getDeletedCount());
    }
}
